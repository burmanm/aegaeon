package fi.iki.yak.aegaeon.input.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import rx.Observable;
import rx.Subscription;

public class WatchServiceAssistTest {

    private WatchServiceAssist assist;

    @Before
    public void setup() {
        assist = new WatchServiceAssist();
    }

    @Ignore
    @Test
    public void testFileEventObservable() throws Exception {
        final LinkedList<FileEvent> fileEventList = new LinkedList<>();
        Path inputPath = Paths.get("src/test/resources/input");
        Observable<FileEvent> observable = assist.getDirWatcher(inputPath.toFile().getAbsolutePath(), ".*"); // @TODO Test regexp filter here?

        // Nothing gets emitted when in JUnit.. puuh.
        Subscription subscribe = observable.subscribe(fileEvent -> {
            System.out.println("Event received");
            fileEventList.add(fileEvent);
        });

        Path file = inputPath.resolve("test.xml");
        Files.createFile(file);

        Thread.sleep(2000);

        subscribe.unsubscribe();

        int listSize = fileEventList.size();
        Files.delete(file);
        assertEquals(1, listSize);
    }

    private FileEvent createEmptyFileEvent(String key, FileEvent.Type type) {
        Path testPath = Paths.get("empty");
        FileEvent event = new FileEvent(key, testPath);
        event.setEventType(type);
        return event;
    }

    @Test
    public void testRenameDetection() {
        List<FileEvent> fillList = new LinkedList<>();
        fillList.add(createEmptyFileEvent("rename", FileEvent.Type.DELETED));
        fillList.add(createEmptyFileEvent("rename", FileEvent.Type.CREATED));
        List<FileEvent> duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be renamed", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.RENAMED);

        assertNotNull("There should be the old path also in rename events", duplicateEventsFilteredList.get(0).getOldPath());
    }

    @Test
    public void testCreatedDetection() {
        List<FileEvent> fillList = new LinkedList<>();
        fillList.add(createEmptyFileEvent("created", FileEvent.Type.CREATED));
        List<FileEvent> duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be created", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.CREATED);
    }

    @Test
    public void testDeleted() {
        List<FileEvent> fillList = new LinkedList<>();
        fillList.add(createEmptyFileEvent("deleted", FileEvent.Type.CREATED));
        fillList.add(createEmptyFileEvent("deleted", FileEvent.Type.DELETED));
        List<FileEvent> duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be deleted", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.DELETED);

        fillList.clear();
        fillList.add(createEmptyFileEvent("deleted2", FileEvent.Type.CREATED));
        fillList.add(createEmptyFileEvent("deleted2", FileEvent.Type.MODIFIED));
        fillList.add(createEmptyFileEvent("deleted2", FileEvent.Type.DELETED));

        duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be deleted", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.DELETED);

        fillList.clear();
        fillList.add(createEmptyFileEvent("deleted3", FileEvent.Type.DELETED));

        duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be deleted", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.DELETED);
    }

    @Test
    public void testModifiedRemovals() {
        List<FileEvent> fillList = new LinkedList<>();
        fillList.add(createEmptyFileEvent("modified", FileEvent.Type.CREATED));
        fillList.add(createEmptyFileEvent("modified", FileEvent.Type.MODIFIED));
        List<FileEvent> duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be created", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.CREATED);

        fillList.clear();
        fillList.add(createEmptyFileEvent("modified2", FileEvent.Type.RENAMED));
        fillList.add(createEmptyFileEvent("modified2", FileEvent.Type.MODIFIED));

        duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be renamed", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.RENAMED);

        fillList.clear();
        fillList.add(createEmptyFileEvent("modified", FileEvent.Type.MODIFIED));

        duplicateEventsFilteredList = assist.removeDuplicateEvents(fillList);

        assertEquals("There should be only one item in the list", 1, duplicateEventsFilteredList.size());
        assertTrue("Event type should be modified", duplicateEventsFilteredList.get(0).getEventType() == FileEvent.Type.MODIFIED);
    }
}
