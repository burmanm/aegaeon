//package fi.iki.yak.aegaeon.input.file;
//
//import org.junit.After;
//import org.junit.Ignore;
//import org.junit.Test;
//import rx.Observable;
//import rx.Subscriber;
//
//import java.io.BufferedWriter;
//import java.io.IOException;
//import java.nio.file.Files;
//import java.nio.file.Path;
//import java.nio.file.Paths;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Properties;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//import static org.junit.Assert.fail;
//
//public class FileReaderServiceTest {
//
//    private final static String TEST_PATH = "src/test/resources/input";
//
//    @Ignore
//    @Test
//    public void testReadingFromBeginning() {
//        Properties properties = getPropertiesStub();
//        properties.setProperty(FileReaderService.PROPERTIES_FROM_BEGINNING, "true");
//        FileReaderService service = getService(properties);
//    }
//
//    @Ignore
//    @Test
//    public void testReadingFromEnd() throws Exception {
//        Properties properties = getPropertiesStub();
//        properties.setProperty(FileReaderService.PROPERTIES_FROM_BEGINNING, "false");
//        FileReaderService service = getService(properties);
//
//        Path file = Files.createFile(Paths.get(TEST_PATH, "test.log"));
//
//        BufferedWriter bufferedWriter = Files.newBufferedWriter(file);
//        bufferedWriter.write("hep hep hep");
//        bufferedWriter.write('\n');
//
//        final List<String> receivedEvents = new LinkedList<>();
//
////        service.getLineEvents(TEST_PATH, ".*").subscribe(s -> receivedEvents.add(s));
//
//        bufferedWriter.write("hellurei");
//        bufferedWriter.write('\n');
//        bufferedWriter.close();
//
//        Files.deleteIfExists(file);
//
//        Thread.sleep(2000);
//        assertEquals(2, receivedEvents.size());
//    }
//
//    @Test
//    public void testEventHandling() {
//
//    }
//
//    @Test
//    public void testEventAssistingMethodsAdd() throws IOException {
//        Properties properties = getPropertiesStub();
//        properties.setProperty(FileReaderService.PROPERTIES_FROM_BEGINNING, "true");
//        FileReaderService service = getService(properties);
//
//        FileEvent event = createEmptyFileEvent("first", FileEvent.Type.CREATED);
//
//        // Test adding with start position 0
//        service.trackingStart(event);
//        assertEquals("We should be tracking one file", 1, service.getTrackedFiles().size());
//        assertEquals("We should be starting from the beginning", 0, service.getTrackedFiles().get(0).getPosition());
//
//
//    }
//
//    @Test
//    public void testEventAssistingMethodsRemove() throws IOException {
//        FileReaderService service = getService(null);
//        FileEvent event = createEmptyFileEvent("first", FileEvent.Type.CREATED);
//
//        service.trackingStart(event);
//        service.trackingRemoved(event);
//        assertEquals("We shouldn't be tracking anything", 0, service.getTrackedFiles().size());
//    }
//
//    @Test
//    public void testEventAssistingMethodsModified() throws IOException {
//        FileReaderService service = getService(getPropertiesStub());
//        FileEvent event = createEmptyFileEvent("first", FileEvent.Type.MODIFIED);
//
//        service.trackingModified(event);
//        assertEquals("We should be tracking one file", 1, service.getTrackedFiles().size());
//    }
//
//    @Test
//    public void testEventAssistingMethodsRenaming() throws IOException {
//        Path first = null;
//        try {
////            FileReaderService service = getService(null);
//            FileEvent event = createEmptyFileEvent("first", FileEvent.Type.CREATED);
//
//            service.trackingStart(event);
//
//            first = Paths.get("first");
//            Files.createFile(first);
//            FileEvent modified = new FileEvent("first", first);
//
//            modified.setOldPath(event.getFilePath()); // Set the previous one
//            service.trackingRenamed(modified);
//
//            assertEquals("We should be tracking one file", 1, service.getTrackedFiles().size());
//            assertTrue("We should be tracking the new file", service.getTrackedFiles().get(0).getFilePath().equals(first));
//
//            event.setOldPath(Paths.get("second"));
//            service.trackingRenamed(event); // This isn't tracked yet.. should be added
//            assertEquals("We should be tracking two files", 2, service.getTrackedFiles().size());
//
//        } catch (IOException e) {
//            fail("IOException while executing test " + e.getLocalizedMessage());
//        } finally {
//            Files.deleteIfExists(first);
//        }
//    }
//
//    // Helper methods
//
//    private FileReaderService getService() {
//        return new FileReaderService()
//    }
//
////    private FileReaderService getService(Properties properties) {
////        if(properties == null) {
////            properties = getPropertiesStub();
////        }
////        return new FileReaderService(properties);
////    }
//
//    private Properties getPropertiesStub() {
//        Properties properties = new Properties();
//        properties.put(FileReaderService.PROPERTIES_DIRECTORY, TEST_PATH);
//        properties.put(FileReaderService.PROPERTIES_REGEXP_RULE, ".*");
//        properties.put(FileReaderService.PROPERTIES_FROM_BEGINNING, "false");
//        return properties;
//    }
//
//    private Observable<FileEvent> getStubObservable() {
//        return Observable.create(new Observable.OnSubscribe<FileEvent>() {
//            @Override
//            public void call(Subscriber<? super FileEvent> subscriber) {
//
//            }
//        });
//    }
//
//    private FileEvent createEmptyFileEvent(String key, FileEvent.Type type) throws IOException {
//        Path testPath = Paths.get(TEST_PATH, "empty");
//        if(!Files.exists(testPath)) {
//            Files.createFile(testPath);
//        }
//        FileEvent event = new FileEvent(key, testPath);
//        event.setEventType(type);
//        return event;
//    }
//
//    @After
//    public void cleanUp() throws IOException {
//        Path testPath = Paths.get(TEST_PATH, "empty");
//        Files.deleteIfExists(testPath);
//    }
//}
