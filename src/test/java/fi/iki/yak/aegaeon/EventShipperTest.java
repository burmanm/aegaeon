//package fi.iki.yak.aegaeon;
//
//import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
//import fi.iki.yak.aegaeon.buffer.Buffer;
//import fi.iki.yak.aegaeon.buffer.InMemoryBuffer;
//import org.junit.Test;
//import rx.Observable;
//import rx.Observer;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class EventShipperTest {
//
//    /**
//     * Simple test to see that items sent to the eventShipper are also received at the other end
//     */
//    @Test
//    public void testShipper() {
//
//        final List<String> sentItems = new LinkedList<>();
//        final List<String> receivedItems = new LinkedList<>();
//
//        Observer<String> observer = new Observer<String>() {
//            @Override
//            public void onCompleted() {
//
//            }
//
//            @Override
//            public void onError(Throwable e) {
//
//            }
//
//            @Override
//            public void onNext(String args) {
//                receivedItems.add(args);
//            }
//        };
//
//        Observable<String> from = Observable.from(receivedItems);
//        Buffer<String> buffer = new InMemoryBuffer();
//
//        EventShipper eventShipper = new EventShipper(observer, from, buffer);
//        eventShipper.run();
//
//        assertThat("We should have same items in both lists", receivedItems, is(sentItems));
//    }
//}
