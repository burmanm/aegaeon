package fi.iki.yak.aegaeon;

import fi.iki.yak.aegaeon.buffer.Buffer;
import fi.iki.yak.aegaeon.input.EventEmitter;
import fi.iki.yak.aegaeon.output.EventProcessor;
import rx.Observable;
import rx.Observer;
import rx.Subscription;

public class EventShipper implements Runnable {

    private Subscription input;
    private Subscription output;

    private final Buffer<String> eventBuffer;
	private final EventProcessor<String> observer;
	private final EventEmitter<String> observable;

    /**
     * Inject the correct observer (AMQP / JMS) here. This is stub for now..
     */
    public EventShipper(EventEmitter<String> emitter, EventProcessor<String> processor, Buffer<String> eventBuffer) {
        this.observable = emitter;
        this.observer = processor;
        this.eventBuffer = eventBuffer;

        // @TODO Could use iterableObserver etc, that could indicate if our observer can do backpressure.. instead of our own queue implementation..
        // @TODO Or should these be in the run() ?
        this.input = emitter.getEvents().subscribe(eventBuffer.getSubscriber()); // Transfer incoming events to the buffer
        this.output = eventBuffer.observeEvents().subscribe(observer.getSubscriber());
    }

    /**
     * Create async observable for buffer reading. Observables allow throttling also, if
     * that becomes an issue.
     *
     * @return
     */
    @Override
    public void run() {

    }

    // @TODO Add MBean methods here? Instead of letting them to be accessed directly?

    public void close() {
        input.unsubscribe();
        // @TODO Wait for the output to process everything from the buffer?
        output.unsubscribe();
    }

}
