package fi.iki.yak.aegaeon.buffer;

import java.util.concurrent.LinkedBlockingQueue;

import rx.Observable;
import rx.Subscriber;

/**
 * This class implements a simple in-memory buffer using the LinkedBlockingQueue
 */
public class InMemoryBuffer implements Buffer<String> {

    private final LinkedBlockingQueue<String> lineQueue;

    public InMemoryBuffer() {
        lineQueue = new LinkedBlockingQueue<>();
    }

    @Override
    public Subscriber<String> getSubscriber() {
        return new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(String s) {
                queue(s);
            }
        };
    }

    @Override
    public void queue(String event) {
        // Make this async.. as in not blocking?
        lineQueue.offer(event);
    }

    @Override
    public Observable<String> observeEvents() {
        return Observable.create((Subscriber<? super String> subscriber) -> {
            // @TODO Remove threading and use subscribeOn / observeOn?
            final Thread t = new Thread(() -> {
                while(!subscriber.isUnsubscribed()) {
                    String line;
                    try {
                        line = lineQueue.take();
                    } catch (InterruptedException e) {
                        continue;
                    }

                    if(line != null) {
                        subscriber.onNext(line);
                    }
                }
            });
            t.setDaemon(true);
            t.start();
        });
    }

    @Override
    public Long queueSize() {
        return Long.valueOf(lineQueue.size());
    }
}
