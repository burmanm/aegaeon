package fi.iki.yak.aegaeon.buffer;

import rx.Observable;
import rx.Subscriber;

/**
 * A simple interface to abstract the buffer implementations
 */
public interface Buffer<T> {
    public Subscriber<T> getSubscriber();
    public void queue(T event); // What if something fails?
    public Observable<T> observeEvents();
    public Long queueSize(); // Amount of queued items
}
