package fi.iki.yak.aegaeon;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.WatchService;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import fi.iki.yak.aegaeon.buffer.InMemoryBuffer;
import fi.iki.yak.aegaeon.input.file.FileReaderService;
import fi.iki.yak.aegaeon.input.file.WatchServiceAssist;
import fi.iki.yak.aegaeon.output.qpid.QpidProtonSender;

/**
 * @TODO Add throttling to AMQP?
 * @TODO Support using as a library instead of standalone program
 * @TODO Properties parsing and handling
 * @TODO Error handling is buggy.. exceptions are eaten by RxJava & something else.
 * @TODO Fix logging
 *
 * The plan:
 *
 *  - create single instance of DirWatcher (one can't create multiple, or can I? Need to examine..)
 *    - create instance of protonSender & FileReaderService.Observer for each dirwatcher..
 *    - If possible to generate multiple.. then here answer with MBeans (keep track of all watchers / senders etc)
 */
public class Aegaeon {

    private WatchServiceAssist watchServiceAssist;

    private List<EventShipper> eventShipperList;

	public Aegaeon() {
		watchServiceAssist = new WatchServiceAssist();
        eventShipperList = new LinkedList<>();
	}

    public Properties loadProperties(String path) throws IOException {
        Properties properties = new Properties();
        FileInputStream fis = new FileInputStream(path);
        properties.load(fis);
//        properties.load(this.getClass().getResourceAsStream("/aegaeon.properties"));
        fis.close();
        return properties;
    }

    /**
     * When called from the command line (standalone)
     *
     * @param properties
     */
    public void startWatching(Properties properties) {
        boolean fromBeginning = Boolean.valueOf(properties.getProperty(FileReaderService.PROPERTIES_FROM_BEGINNING, "false"));
        String path = properties.getProperty(FileReaderService.PROPERTIES_DIRECTORY);
        String regexp = properties.getProperty(FileReaderService.PROPERTIES_REGEXP_RULE);
        String destAddress = properties.getProperty(QpidProtonSender.PROPERTIES_ADDRESS);
        startWatching(fromBeginning, path, regexp, destAddress);
    }

    /**
     * Start watching a filepath for new events and transfer them to the destination queue.
     *
     * @param fromBeginning
     * @param path
     * @param regexp
     * @param destAddress
     */
    public void startWatching(boolean fromBeginning, String path, String regexp, String destAddress) {
        FileReaderService service = new FileReaderService(watchServiceAssist.getDirWatcher(path, regexp), fromBeginning);
        QpidProtonSender sender = new QpidProtonSender(destAddress);

        // @TODO Replace InMemoryBuffer with proper backpressure support?
        // @TODO Only give interfaces, and let it fetch the observers based on those. Allows easier monitoring implementation.
        // @TODO Let EventShipper also handle the threading?
        EventShipper eventShipper = new EventShipper(service, sender, new InMemoryBuffer());
        eventShipperList.add(eventShipper);
        Thread t = new Thread(eventShipper);
        t.setDaemon(false);
        t.start();
    }

    // @TODO Register one new MBean for each eventShipper? Under some structure.. so one could monitor each of them..
//    public void registerMBean() {
//        MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
//        try {
//            ObjectName aegaeonName = new ObjectName("fi.iki.yak.aegaeon:type=Manage");
//            AegaeonMBean aegaeonMBean = new AegaeonMBeanImpl();
//            StandardMBean mbean = new StandardMBean(aegaeonMBean, AegaeonMBean.class);
//            mbs.registerMBean(mbean, aegaeonName);
//        } catch (MalformedObjectNameException e) {
//            e.printStackTrace();
//        } catch (NotCompliantMBeanException e) {
//            e.printStackTrace();
//        } catch (InstanceAlreadyExistsException e) {
//            e.printStackTrace();
//        } catch (MBeanRegistrationException e) {
//            e.printStackTrace();
//        }
//    }

	public static void main(String[] args) {

        if(args.length < 1) {
            System.out.println("Usage: aegaeon <properties.file>");
            System.exit(1);
        } else {
            String propertiesFile = args[0];
            Aegaeon aegaeon = new Aegaeon();

            try {
                Properties properties = aegaeon.loadProperties(propertiesFile);
                aegaeon.startWatching(properties);

                System.out.println("Started processing logs..");
                Thread.sleep(Long.MAX_VALUE);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(1);
            } catch (InterruptedException e) {
                System.exit(1);
            }
        }
	}

}
