package fi.iki.yak.aegaeon.jmx;

import java.util.List;

/**
 * JMX bean to return some basic status for monitoring purposes
 * @TODO JMX MBean to support operations: shutdown, shutdown with pooling emptying, stopping reader, stopping sender
 * @TODO JMX MBean to support statistics: queue size, performance (events per second)
 * @TODO JMX MBean to support info: list of tracked files
 * @TODO Support modifying tracked files (add / remove)
 *
 * @author michael
 *
 */
public interface AegaeonMBean {
    // Information
    public Long getQueueSize(); // @TODO Redo after backpressure support..
    public List<String> getTrackedFiles();

    // Operations
    public void shutdown(boolean emptyFirst);
    public void stopReader();
    public void stopSender();
    public void startReader();
    public void startSender();

    public boolean isReaderStarted();
    public boolean isSenderStarted();

    public void trackFilePath(String path);
    public void removeTracking(String path);
}
