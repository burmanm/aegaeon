package fi.iki.yak.aegaeon.input.file;

import java.nio.file.Path;

/**
 * Describes an event happening in the filesystem
 */
public class FileEvent {

    public enum Type { CREATED, MODIFIED, DELETED, RENAMED }

    private Object fileKey;
    private final Path filePath;
    private Path oldPath;
    private Type eventType;

    public FileEvent(Object fileKey, Path filePath) {
        this.fileKey = fileKey;
        this.filePath = filePath;
    }

    public Object getFileKey() {
        return fileKey;
    }

    public void setFileKey(Object fileKey) {
        this.fileKey = fileKey;
    }

    public Path getFilePath() {
        return filePath;
    }

    public Type getEventType() {
        return eventType;
    }

    public void setEventType(Type eventType) {
        this.eventType = eventType;
    }

    public Path getOldPath() {
        return oldPath;
    }

    public void setOldPath(Path oldPath) {
        this.oldPath = oldPath;
    }
}
