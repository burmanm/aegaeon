package fi.iki.yak.aegaeon.input.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fi.iki.yak.aegaeon.input.EventEmitter;

import org.bitbucket.kienerj.io.OptimizedRandomAccessFile;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Func1;

/**
 * @TODO Remember file charset encoding! If none provided, use platform encoding
 * @TODO FileVisitor to check existing files..? Or only on modified events? Recursive? - Has to be .. otherwise we miss the first event
 * @TODO Backpressure using RxJava? Just try to cache enough so that we don't loose events in case of file renames etc..
 *
 * @author michael
 *
 */
public class FileReaderService implements EventEmitter<String> {

    public final static String PROPERTIES_DIRECTORY = "file.directory";
    public final static String PROPERTIES_REGEXP_RULE = "file.rule";
    public final static String PROPERTIES_FROM_BEGINNING = "file.position.beginning";

    private boolean startFromBeginning = false;

    private final Map<String, FileStatus> channelMap;
    private Observable<FileEvent> fileEventObservable;

    public FileReaderService(Observable<FileEvent> fileEventObservable, boolean startFromBeginning) {
        this.startFromBeginning = startFromBeginning;
        this.fileEventObservable = fileEventObservable;
        channelMap = new HashMap<>();
    }

    // public methods

    /**
     * Emit each new line in the file as new observed event
     *
     * @param status
     * @return
     */
    public Observable<String> readFileLines(final FileStatus status) {
        return Observable.create((Subscriber<? super String> subscriber) -> {
            OptimizedRandomAccessFile file = null;
            try {
                long startPosition = status.getPosition();
                file = new OptimizedRandomAccessFile(status.getFilePath().toFile(), "r");

                if(file.length() != startPosition) {
                    if(file.length() < startPosition) {
                        // File was truncated, start from the beginning
                        startPosition = 0;
                    }
                    file.seek(startPosition);

                    String line;
                    while ((line = file.readLine()) != null) {
                        subscriber.onNext(line);
                    }
                    status.setPosition(file.getFilePointer());
                    trackingUpdate(status); // I wish this could be done in the onComplete process, but it looks ugly
                }
                subscriber.onCompleted();
            } catch (IOException e) {
                subscriber.onError(e);
            } finally {
                if(file != null) {
                    try {
                        file.close();
                    } catch (IOException e) {
//                            e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    public Observable<String> getEvents() {
        return fileEventObservable.flatMap(handleFileEvents());
    }

    // private methods

    /**
     * Operations:
     - on create, add new file to be read, set position as 0 (start from the beginning)
     - read until at the end, wait for modify later
     - update last known position after reading
     - on modify
     - read until the end from the last known position
     - update last known position
     - on rename
     - modify the old path to a new one
     - read until the end
     - if fileFilter doesn't match anymore

     * @return
     */
    private Func1<FileEvent, Observable<? extends String>> handleFileEvents() {

        return fileEvent -> {
            switch(fileEvent.getEventType()) {
                case CREATED:
                    // Start tracking?
                    return readFileLines(trackingStart(fileEvent));
                case MODIFIED:
                    return readFileLines(trackingModified(fileEvent));
                case RENAMED:
                    return readFileLines(trackingRenamed(fileEvent));
                case DELETED:
                    trackingRemoved(fileEvent);
                    break;
                default:
                    // We'll ignore everything else for now..
                    break;
            }
            return Observable.empty();
        };
    }

    // Methods for monitoring purposes

    public List<FileStatus> getTrackedFiles() {
        return new LinkedList<>(channelMap.values());
    }

    // Some assisting methods, to ease refactoring

    FileStatus trackingStart(FileEvent s) {
        FileStatus status = new FileStatus(s.getFilePath());
        if(!startFromBeginning) {
            try {
                status.setPosition(Files.size(s.getFilePath()));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        channelMap.put(getTrackingKey(s), status);
        return status;
    }

    FileStatus trackingModified(FileEvent fileEvent) {
        FileStatus status = channelMap.get(getTrackingKey(fileEvent));
        if(status == null) {
            // Not present, add it ..
            status = trackingStart(fileEvent);
        }
        return status;
    }

    void trackingRemoved(FileEvent fileEvent) {
        if(channelMap.containsKey(getTrackingKey(fileEvent))) {
            channelMap.remove(getTrackingKey(fileEvent));
        }
    }

    /**
     * @TODO Should not continue tracking if the regexp filter is no more matching.
     * @param fileEvent
     * @return
     */
    FileStatus trackingRenamed(FileEvent fileEvent) {
        String oldKey = getTrackingKey(fileEvent.getOldPath());
        FileStatus status = channelMap.get(oldKey);
        if(status == null) {
            // We weren't tracking this file before.. should we now?
            trackingStart(fileEvent);
        } else {
            channelMap.remove(oldKey);
            // Read lines and then remove if not matching to a filter?
            status.setFilePath(fileEvent.getFilePath());
            channelMap.put(getTrackingKey(fileEvent), status);
        }
        return status;
    }

    private String getTrackingKey(FileEvent fileEvent) {
        return getTrackingKey(fileEvent.getFilePath());
    }

    private String getTrackingKey(FileStatus status) {
        return getTrackingKey(status.getFilePath());
    }

    private String getTrackingKey(Path path) {
        return path.toString().trim();
    }

    private void trackingUpdate(FileStatus status) {
        channelMap.remove(getTrackingKey(status));
        channelMap.put(getTrackingKey(status), status);
    }
}
