package fi.iki.yak.aegaeon.input.file;

import java.nio.file.Path;

/**
 * This class stores the information of tracked files, path and the last positions
 */
public class FileStatus {

    private Path filePath;
    private long position = 0;

    public FileStatus(Path filePath) {
        this.filePath = filePath;
    }

    public Path getFilePath() {
        return filePath;
    }

    public void setFilePath(Path filePath) {
        this.filePath = filePath;
    }

    public long getPosition() {
        return position;
    }

    public void setPosition(long position) {
        this.position = position;
    }
}
