package fi.iki.yak.aegaeon.input;

import rx.Observable;

/**
 * Simple observable interface to abstract event parsing
 * @param <T>
 */
public interface EventEmitter<T> {
    public Observable<T> getEvents();
}
