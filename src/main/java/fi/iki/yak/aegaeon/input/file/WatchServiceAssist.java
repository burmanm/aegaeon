package fi.iki.yak.aegaeon.input.file;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import rx.Observable;

/**
 * @author michael
 * @TODO Track Windows files with NTFS id (Java 7 doesn't provide direct methods to do this)
 * /**
 * How to detect rename? In Windows, we could store external attribute.. but not on all Linux filesystems.
 * <p/>
 * BasicFileAttributes + fileKey (get inode identifier)
 * <p/>
 * Path file = ...
 * BasicFileAttributes attrs = Files.readAttributes(file, BasicFileAttributes.class);
 * <p/>
 * Store these uuids somewhere and check when rename happens.. so we won't monitor the same file
 * many times.
 *
 * @TODO Remove threading here.. and add following methods: startWatching(Path) (FileVisitor this one), stopWatching(Path)
 */
public class WatchServiceAssist {

    public Observable<FileEvent> getDirWatcher(final String dir, final String regexp) {
        return Observable.create((Observable.OnSubscribe<FileEvent>) subscriber -> {

            // @TODO Don't start thread anymore, instead use RxJava async methods for creating new thread?
            final Thread t = new Thread(() -> {
                try {
                    Path directory = Paths.get(dir);
                    WatchService watchService = FileSystems.getDefault().newWatchService();
                    WatchKey register = directory.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
                            StandardWatchEventKinds.ENTRY_DELETE,
                            StandardWatchEventKinds.ENTRY_MODIFY);

                    final Pattern pattern = Pattern.compile(regexp);                    

                    HashMap<String, Object> fileKeyMatcherMap = new HashMap<>();

                    while(!subscriber.isUnsubscribed()) {
                        WatchKey key;
                        try {
                            key = watchService.take();
                        } catch (InterruptedException e) {
                            continue;
                        }

                        List<FileEvent> fileEventList = new LinkedList<>();

                        for (WatchEvent<?> event : key.pollEvents()) {
                            Kind<?> kind = event.kind();
                            if (kind == StandardWatchEventKinds.OVERFLOW) {
                                continue;
                            }

                            @SuppressWarnings("unchecked")
                            WatchEvent<Path> ev = (WatchEvent<Path>) event;
                            Path eventFile = directory.resolve(ev.context());

                            // Read a unique identifier from the file
                            Object fileKey = null;
                            if(Files.exists(eventFile, LinkOption.NOFOLLOW_LINKS)) {
                                BasicFileAttributes attrs = Files.readAttributes(eventFile, BasicFileAttributes.class);
                                fileKey = attrs.fileKey();
                            }

                            // fileKey could be null, because the underlying filesystem did not provide such info.
                            // rename detection is based on the fileKey, and does not work without it unfortunately
                            if (fileKey == null) {
                                // Filesystem could not provide a fileKey, use filename?
                                fileKey = eventFile.getFileName();
                            }
                            FileEvent fileEvent = new FileEvent(fileKey, eventFile);

                            FileEvent.Type type;

                            if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
                                fileKeyMatcherMap.put(fileEvent.getFilePath().toAbsolutePath().toString(), fileEvent.getFileKey());
                                type = FileEvent.Type.CREATED;
                            } else if(kind == StandardWatchEventKinds.ENTRY_DELETE) {
                                // Check if we have existing fileKey, for matching purposes
                                String fetchKey = eventFile.toAbsolutePath().toString();

                                if(fileKeyMatcherMap.containsKey(fetchKey)) {
                                    fileKey = fileKeyMatcherMap.get(fetchKey);
                                    fileEvent.setFileKey(fileKey);
                                    fileKeyMatcherMap.remove(fileKey);
                                }
                                type = FileEvent.Type.DELETED;
                            } else if(kind == StandardWatchEventKinds.ENTRY_MODIFY) {
                                type = FileEvent.Type.MODIFIED;
                            } else {
                                continue;
                            }
                            fileEvent.setEventType(type);
                            fileEventList.add(fileEvent);
                        }

                        Observable.from(removeDuplicateEvents(fileEventList))
                                .filter(fileEvent -> pattern.matcher(fileEvent.getFilePath().getFileName().toString()).matches())
                                .subscribe(fileEvent -> subscriber.onNext(fileEvent));

                        // Now reset the key so we can monitor new changes
                        boolean valid = key.reset();
                        if (!valid) {
                            subscriber.onError(new RuntimeException("Path is no longer valid"));
                            break; // If path is no longer available
                        }
                    }
                    register.cancel();
                    subscriber.onCompleted();
//                            System.out.println("Should have unregistered..");
                } catch (IOException ioe) {
                    subscriber.onError(ioe);
                }
            });
            t.setDaemon(true);
            t.start();
        });
    }

    /**
     * Remove duplicate FileEvents from the list that Watcher returned
     *
     * Java8, streams: First sorted to get same fileevents one after another, then collect to combine and modify them..
     *
     * @param origList
     * @return
     */
    List<FileEvent> removeDuplicateEvents(List<FileEvent> origList) {
        Map<Object, FileEvent> eventMap = new HashMap<>();

        Iterator<FileEvent> fileEventIterator = origList.iterator();
        while(fileEventIterator.hasNext()) {
            FileEvent event = fileEventIterator.next();
            if(eventMap.containsKey(event.getFileKey())) {
                FileEvent existing = eventMap.get(event.getFileKey());
                FileEvent.Type existingType = existing.getEventType();
                switch(event.getEventType()) {
                    case DELETED:
                        eventMap.remove(existing.getFileKey());
                        eventMap.put(event.getFileKey(), event);
                        break;
                    case CREATED:
                        if(existingType == FileEvent.Type.DELETED) {
                            eventMap.remove(existing.getFileKey());
                            event.setEventType(FileEvent.Type.RENAMED);
                            event.setOldPath(existing.getFilePath());
                            eventMap.put(event.getFileKey(), event);
                        } else {
                            eventMap.remove(existing.getFileKey());
                            eventMap.put(event.getFileKey(), event);
                        }
                        break;
                    case MODIFIED:
                        if(existingType == FileEvent.Type.CREATED || existingType == FileEvent.Type.RENAMED) {
                            continue;
                        } else {
                            eventMap.put(event.getFileKey(), event);
                        }
                        break;
                }
            } else {
                eventMap.put(event.getFileKey(), event);
            }
        }
        return new LinkedList<>(eventMap.values());
    }
}
