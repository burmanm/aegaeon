package fi.iki.yak.aegaeon.output.qpid;

import org.apache.qpid.proton.amqp.messaging.AmqpValue;
import org.apache.qpid.proton.message.Message;
import org.apache.qpid.proton.message.MessageFactory;
import org.apache.qpid.proton.message.impl.MessageFactoryImpl;
import org.apache.qpid.proton.messenger.Messenger;
import org.apache.qpid.proton.messenger.MessengerFactory;
import org.apache.qpid.proton.messenger.impl.MessengerFactoryImpl;

import fi.iki.yak.aegaeon.output.EventProcessor;
import rx.Observer;
import rx.Subscriber;

import java.io.IOException;
import java.util.Properties;

/**
 * @TODO Test forever retries in case of losing destination (for example network split)
 */
public class QpidProtonSender implements EventProcessor<String> {

    public static final String PROPERTIES_ADDRESS = "connectionfactory.qpidConnectionFactory";

    private String address;
    private Messenger messenger;
    private Message message; // mutable message implementation

    public QpidProtonSender(String destAddress) {
        MessengerFactory factory = new MessengerFactoryImpl();
        MessageFactory messageFactory = new MessageFactoryImpl();
        messenger = factory.createMessenger();
        message = messageFactory.createMessage();
        address = destAddress;

        try {
            messenger.start();
        } catch (IOException e) {
            throw new RuntimeException("Could not connect messenger to the destination queue");
        }
    }

    private void send(String line) {
        message.setAddress(address);
        message.setBody(new AmqpValue(line));

        messenger.put(message);
        messenger.send();
    }

    public void close() {
        messenger.stop();
    }

    @Override
    public Subscriber<String> getSubscriber() {
        return new Subscriber<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onNext(String s) {
                send(s);
                // if we start failing, we could always unsubscribe for a moment until we're back..
            }
        };
    }
}
