package fi.iki.yak.aegaeon.output;

import rx.Subscriber;

/**
 * Simple interface to abstract the event sending process
 * @param <T>
 */
public interface EventProcessor<T> {
    public Subscriber<T> getSubscriber();
}
